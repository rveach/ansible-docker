# ansible-docker

## About

This image was recently repurposed to run ansible from a CI/CD Pipeline, specifically, Gitlab, which is what I use.

This ships with a run script that will wrap the ansible-playbook executable in a way that will allow:

- ansible-playbook cli flags set with either environment variables or command line flags.
- password protected ssh keys, decrypted with ssh-keygen
- password protected Ansible vault.
- ssh config and known_hosts files stored in the repo.

## Directory Structure

Assumption is that a CI/CD task will clone a playbook into the CWD and run from there.  This has mainly been tested with Gitlab CI.

The following directory structure should be used in playbook repo:

```
playbooks/             # pull all ansible playbook stuff here
    site.yml           # or any other file
    group_vars/        # with group_vars and roles
    roles/             # just do whatever you've configured
                       # follow standard ansible best practices

ssh/                   # ssh info
    id_rsa             # private key - can be named anything
    id_rsa.pub         # keep the public key here too
    known_hosts        # known hosts file
    config             # ssh config file

hosts                  # hosts file
ansible.cfg            # ansible.cfg file
```

## Configuration


## run.py

```
run.py -h
usage: run.py [-h] [-p PLAYBOOKS [PLAYBOOKS ...]] [--pingtest]
              [--playbook-directory PBD] [--ssh-directory SSH_DIRECTORY]
              [--key-privkey KEY_FILENAME] [--key-passphrase KEY_PASSPHRASE]
              [-v VAULTPW] [-d] [-t TIMEOUT] [-c]

optional arguments:
  -h, --help            show this help message and exit
  -p PLAYBOOKS [PLAYBOOKS ...], --playbooks PLAYBOOKS [PLAYBOOKS ...]
  --pingtest
  --playbook-directory PBD
                        set the path to the playbook directory. Can also be
                        set by env variable PLAYBOOK_DIRECTORY
  --ssh-directory SSH_DIRECTORY
  --key-privkey KEY_FILENAME
                        ssh private key within the ssh directory. Also set by
                        env variable SSH_KEY_NAME
  --key-passphrase KEY_PASSPHRASE
                        ssh key password, if required. Also set by env
                        variable SSH_KEY_PASSPHRASE
  -v VAULTPW, --vault-password VAULTPW
                        ansible vault password. Also set by env variable
                        VAULT_PASSWORD
  -d, --debug
  -t TIMEOUT, --timeout TIMEOUT
                        Timeout value when running Ansible. Also set by env
                        variable TIMEOUT.
  -c, --check
```

## Example Gitlab Runner Task

```yaml
# deploy to site
deploy-site:
  stage: deploy
  script:
    - /usr/local/bin/run.py --playbooks site.yml
  only:
    - master
  variables:
    SSH_KEY_NAME: ssh-key-filename
    SSH_KEY_PASSPHRASE: ssh-key-password
    VAULT_PASSWORD: ansible-vault-password
```