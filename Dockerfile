FROM python:3


ADD scripts/run.py /usr/local/bin/run.py
RUN chmod a+x /usr/local/bin/run.py

RUN python -m pip install --no-cache-dir --upgrade pip \
	pip --no-cache-dir --upgrade ansible cryptography ansible-lint
	#pip --no-cache-dir --upgrade molecule ansible mitogen cryptography

# add ansible.cfg, copy if it does not exist
ADD ansible.cfg /etc/ansible/ansible.cfg

CMD ["/usr/local/bin/run.py"]
