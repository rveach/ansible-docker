#! /usr/bin/env python

import argparse
import glob
import os
import shutil
import subprocess
import sys
import tempfile


def default_playbook_dir():
    return os.path.join(os.getcwd(), "playbooks")


def default_ssh_dir():
    return os.path.join(os.getcwd(), "ssh")


def default_hosts_loc():
    return os.path.join(os.getcwd(), "hosts")

def get_v(args_result, env_var_name, default_value):
    """
        This will return a value using the precedence
        cli argument > env var > default_value
        When used, args should have a default value of None
        meant for strings
    """
    if args_result:
        return args_result
    return os.getenv(env_var_name, default=default_value)


def get_l(args_result, env_var_name):
    """
    will return a value using the precedence
    cli argument > env var > []
    when used, args should have a default value that doesn't evlauate to true
    the env variable is split by commas
    """

    # if defined in the args, pass the list
    if args_result:
        return args_result

    # if defined as an env var, split and return the list.
    if env_var_name in os.environ:
        return os.getenv(env_var_name).split(",")

    # else, return an empty list.
    return []


def get_i(args_result, env_var_name, default_value):
    """
        This will return a value using the precedence
        cli argument > env var > default_value
        When used, args should have a default value of None
        meant for integers
    """
    if args_result:
        return args_result
    elif env_var_name in os.environ:
        return int(os.getenv(env_var_name))
    else:
        return None


def get_b(args_result, env_var_name, default_value):
    """
        This will return a value using the precedence
        cli argument > env var > default_value
        When used, args should have a default value of None
        meant for booleans
    """
    if args_result != None:
        return args_result
    elif env_var_name in os.environ:
        var_val = os.getenv(env_var_name).lower()
        if var_val in ["true", "yes"]:
            return True
        elif var_val in ["no", "false"]:
            return False
        else:
            return default_value
    else:
        return default_value


class AnsibleEnvironment:

    @staticmethod
    def install_galaxy_role(galaxy_role):
        """ install a single galaxy role by name """
        print("install", galaxy_role, "here")
        # subprocess.check_call(["ansible-galaxy", "install", galaxy_role])

    def key_permissions(self):
        """ set the ssh keys appropriately """

        if self.debug:
            print(f"Setting permissions of the {self.ssh_dir} directory")
        
        os.chmod(self.ssh_dir, 500)
        for ssh_file in glob.glob(os.path.join(self.ssh_dir, "*")):
            os.chmod(ssh_file, 400)

        # Create default ssh directory
        def_ssh_dir = os.path.join(os.getenv("HOME"), ".ssh")
        if not os.path.isdir(def_ssh_dir):
            os.makedirs(def_ssh_dir, mode=0o700)

        # copy the known hosts file
        known_hosts_src = os.path.join(self.ssh_dir, "known_hosts")
        if os.path.isfile(known_hosts_src):
            known_hosts_dst = os.path.join(def_ssh_dir, "known_hosts")
            shutil.copyfile(known_hosts_src, known_hosts_dst)
            os.chmod(known_hosts_dst, 400)
            

        # copy the ssh config file
        config_src = os.path.join(self.ssh_dir, "config")
        if os.path.isfile(config_src):
            config_dst = os.path.join(def_ssh_dir, "config")
            shutil.copyfile(config_src, config_dst)
            os.chmod(config_dst, 400)

    def set_ssh_deploy_key(self):
        """ return a location on the filesystem where we can find an unencrypted ssh key """

        # if initialized with a passphrase
        if self.ssh_key_passphrase:

            # create a temporary file
            self.ssh_deploy_key = tempfile.mkstemp(
                prefix="deploykey",
                dir=self.ssh_dir,
                text=True,
            )[1]

            # copy from the source protected file to the temp file
            with open(os.path.join(self.ssh_dir, self.ssh_key_name), "r") as source_key_fd:
                with open(self.ssh_deploy_key, "w") as dest_key_fd:
                    dest_key_fd.write(source_key_fd.read())
            
            # call ssh-keygen to remove the passphrase from our temp file
            cmd = [
                'ssh-keygen',
                '-p',
                '-P',
                self.ssh_key_passphrase,
                '-N',
                '',
                '-f',
                self.ssh_deploy_key,
            ]
            subprocess.run(cmd, check=True, timeout=2)

        # if we don't have a passphrase set, just assume we can use what we have.
        else:
            self.ssh_deploy_key = os.path.abspath(
                os.path.join(
                    self.ssh_dir,
                    self.ssh_key_name,
                )
            )

        if self.debug:
            print(f"Using ssh key {self.ssh_deploy_key}.")

        # raise an exception if we can't find this file.
        if not os.path.exists(self.ssh_deploy_key):
            raise FileNotFoundError("Could not locate ssh deploy key.")

    def run_playbook(self, playbooks):
        """ run a playbook by name """

        cmd = ['ansible-playbook', '-i', self.inventory, '--private-key', self.ssh_deploy_key]

        if self.ansible_check:
            cmd.append("--check")

        if self.debug:
            cmd.append("-vv")

        if self.vaultpw:

            # create ansible vault password file
            with tempfile.NamedTemporaryFile() as vault_pw_file:
                vault_pw_file.write(bytes(self.vaultpw, "utf-8"))
                vault_pw_file.flush()

                # add vault password file to command
                cmd = cmd + ["--vault-id", vault_pw_file.name] + playbooks

                # run ansible
                if self.debug:
                    print(cmd)
                    #print(f"Running: {' '.join(cmd)}")
                playbook_execute = subprocess.run(cmd, timeout=self.timeout, cwd=self.playbook_directory)


        else:

            cmd = cmd + playbooks

            if self.debug:
                print(f"Running: {' '.join(cmd)}")
            playbook_execute = subprocess.run(cmd, timeout=self.timeout, cwd=self.playbook_directory)


        return playbook_execute.returncode

    def close(self):
        if self.ssh_key_passphrase:
            if self.ssh_deploy_key:
                os.remove(self.ssh_deploy_key)
                self.ssh_deploy_key = None

    def dump_debug(self):

        if self.ssh_key_passphrase:
            ssh_kp_set = True
        else:
            ssh_kp_set = False
        if self.vaultpw:
            vp_set = True
        else:
            vp_set = False

        print("Config:")
        print(f"- Verbose mode: {self.debug}")
        print(f"- Timeout: {self.timeout}")
        print(f"- Playbook Directory: {self.playbook_directory}")
        print(f"- Playbook Directory exists: {os.path.isdir(self.playbook_directory)}")
        print(f"- SSH Key Dir: {self.ssh_dir}")
        print(f"- SSH Key Dir exists: {os.path.isdir(self.ssh_dir)}")
        print(f"- SSH Private Key Name: {self.ssh_key_name}")
        print(f"- SSH Private Key Password set: {ssh_kp_set}")
        print(f"- Ansible Vault Password set: {vp_set}")
        print(f"- Inventory File: {self.inventory}")
        print(f"- Inventory File exists: {os.path.isfile(self.inventory)}")

    def __init__(
        self,
        playbook_directory=default_playbook_dir(),
        ssh_dir=default_ssh_dir(),
        ssh_key_name="id_rsa",
        ssh_key_passphrase=None,
        debug=True,
        timeout=3600,
        ansible_check=False,
        vaultpw=None,
        inventory=None,
        galaxy_roles=[],
    ):

        self.debug = debug
        self.timeout = timeout
        self.playbook_directory = playbook_directory
        self.ssh_dir = ssh_dir
        self.ssh_key_name = ssh_key_name
        self.ssh_key_passphrase = ssh_key_passphrase
        self.ansible_check = ansible_check
        self.vaultpw = vaultpw
        self.inventory = os.path.abspath(inventory)

        self.key_permissions()
        self.set_ssh_deploy_key()

        if galaxy_roles:
            [self.install_galaxy_role(x) for x in galaxy_roles]


        if self.debug:
            self.dump_debug()


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("-p", "--playbooks", nargs="+", dest="playbooks")
    
    parser.add_argument("--playbook-directory", dest="pbd", default=None,
        help="set the path to the playbook directory.  Can also be set by env variable PLAYBOOK_DIRECTORY")
    
    parser.add_argument("--ssh-directory", dest="ssh_directory", default=None)

    parser.add_argument("--key-privkey", dest="key_filename", default=None,
        help="ssh private key within the ssh directory.  Also set by env variable SSH_KEY_NAME")

    parser.add_argument("--key-passphrase", dest="key_passphrase", default=None,
        help="ssh key password, if required.  Also set by env variable SSH_KEY_PASSPHRASE")

    parser.add_argument("-v", "--vault-password", dest="vaultpw", default=None,
        help="ansible vault password.  Also set by env variable VAULT_PASSWORD")

    parser.add_argument("-d", "--debug", dest="debug", action="store_true", default=False)
    parser.add_argument("-t", "--timeout", dest="timeout", type=int, default=None,
        help="Timeout value when running Ansible.  Also set by env variable TIMEOUT.")

    parser.add_argument("-c", "--check", dest="check", action="store_true", default=False)

    parser.add_argument("-i", "--inventory", dest="inventory", default=None,
        help="Full path to the inventory file.  Also set by env variable HOSTFILE.")

    parser.add_argument("-g", "--galaxy", dest="galaxy_roles", default=[], nargs="*",
        help="Galaxy Roles to install prior to execution. Also set with csv values in GALAXY_ROLES",
    )

    args = parser.parse_args()


    ansible_env = AnsibleEnvironment(
        playbook_directory=get_v(args.pbd, "PLAYBOOK_DIRECTORY", default_playbook_dir()),
        ssh_dir=get_v(args.ssh_directory, "SSH_DIRECTORY", default_ssh_dir()),
        ssh_key_name=get_v(args.key_filename, "SSH_KEY_NAME", "id_rsa"),
        ssh_key_passphrase=get_v(args.key_passphrase, "SSH_KEY_PASSPHRASE", None),
        debug=args.debug,
        timeout=get_i(args.timeout, "TIMEOUT", 3600),
        ansible_check=args.check,
        vaultpw=get_v(args.vaultpw, "VAULT_PASSWORD", None),
        inventory=get_v(args.inventory, "INVENTORY", os.path.join(os.getcwd(), "hosts")),
        galaxy_roles=get_l(args.galaxy_roles, "GALAXY_ROLES"),
    )

    if args.playbooks:
        ansible_exit_code = ansible_env.run_playbook(args.playbooks)
    
    ansible_env.close()
    
    if not args.check:
        sys.exit(ansible_exit_code)
